# 3ds-build-image

Docker image for building Nintendo 3DS applications/libraries locally or in a CI.

Have fun,<br>
Holger Zahnleiter<br>
2021-06-04

## What is in the Docker image?

*  Debian 10 "Buster"
*  3DS toolchain (GCC, 3DS libraries, 3DS tools)
*  Catch2 testing framework
*  Clang 12 for compiling and testing the (platform independent part of the) project on the host
*  CMake and GNU Make as build tools
*  ImageMagick `convert` for image conversion
*  gcov/lcov for determining the test coverage
*  ...

## Build the docker image locally

To build a Docker image, that will be place in your local Docker registry, invoke the following un the command line:

```bash
make image
```

## Build 3DS project in a Docker container

You can, of cause, clone or download your project from inside the container.
Then you can compile it with the 3DS toolchain inside that container.
However, I am demonstrating here how to hold your project on your host computer, map it into the Docker container and compile it inside that container.

```bash
docker run -it --volume <host-dir>:<target-dir>:rw <image-id> bash
```

where `<host-dir>` is your local directory containing the project files,
`<target-dir>` is the desired directory within the container (e.g. `/home/test`) and
`<image-id>` is the ID of this Docker image (you should have build locally and have it in the local Docker registry).

Inside the container `cd <target-dir>` and there you can build you 3DS project.

One example (assuming you are currently workng in the directory containing your code):

```bash
docker run -it --volume `pwd`:/home/dev:rw 3ds-build-image:1.0.0 bash
```

## Using the build image in GitLab CI

The image is getting pushed in my own GitLab Docker registry:

```bash
make push
```

My 3DS projects can use it for CI builds by adding the following to my `.gitlab-ci.yml`:

```yaml
image: registry.gitlab.com/hzahnlei/3ds-build-image/3ds-build-image:1.0.0
```

If you want to use that image in your GitLab account then:

*  you may adjust the `Makefile` to hold your GitLab pathes and
*  push the image into your GitLab registry and adapt your `.gitlab-ci.yml` to reflect your path accordingly.

## Credits

I am using a lot of open source and/or free software and services.
I am sure I forgot one or the other, nevertheless a big thanks to:

*  devkitPro (devkitARM with 3DS libraries)
*  Docker
*  GitLab
*  Linux community
*  CMake
*  GNU C++ and other tools
*  ImageMagick
*  ...

## Disclaimer

"Nintendo" and "Nintendo 3DS" are trademarks of Nintendo (see for example https://trademarks.justia.com/850/80/nintendo-85080097.html).

This project is in no way associated with Nintendo.
It is a personal, free, open source and non-profit project.

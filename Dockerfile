#
# 3DS Build Image
#
# Holger Zahnleiter, 2021-06-01
#

ARG BASE_IMAGE
ARG BASE_IMAGE_TAG
ARG TARGET_IMAGE_TAG

FROM ${BASE_IMAGE}:${BASE_IMAGE_TAG}

LABEL maintainer="Holger Zahnleiter <opensource@holger.zahnleiter.org>" \
    org.label-schema.vendor="Holger Zahnleiter" \
    org.label-schema.name="Linux image for building Nintendo 3DS home-brew in CI/CD environments" \
    org.label-schema.license="MIT" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.version="${BASE_IMAGE_TAG}:${TARGET_IMAGE_TAG}" \
    org.label-schema.schema-version="1.0.0-rc.1" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vcs-type="git" \
    org.label-schema.vcs-url="https://gitlab.com/hzahnlei/3ds-build-image"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y --no-install-recommends apt-utils && \
    apt-get install -y --no-install-recommends sudo ca-certificates pkg-config curl wget bzip2 xz-utils make git bsdtar doxygen gnupg && \
    apt-get install -y --no-install-recommends gdebi-core && \
    apt-get install -y --no-install-recommends cmake && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/devkitPro/pacman/releases/latest/download/devkitpro-pacman.amd64.deb && \
    gdebi -n devkitpro-pacman.amd64.deb && \
    rm devkitpro-pacman.amd64.deb && \
    dkp-pacman -Scc --noconfirm

ENV DEVKITPRO=/opt/devkitpro
ENV PATH=${DEVKITPRO}/tools/bin:$PATH

RUN ln -s /proc/self/mounts /etc/mtab && \
    dkp-pacman -Syyu --noconfirm 3ds-dev nds-dev nds-portlibs && \
    dkp-pacman -S --needed --noconfirm `dkp-pacman -Slq dkp-libs | grep '^3ds-'` && \
    dkp-pacman -Scc --noconfirm
ENV DEVKITARM=${DEVKITPRO}/devkitARM

RUN apt-get update && \
    apt-get -y install apt-transport-https ca-certificates gnupg software-properties-common wget
RUN apt-get -y install build-essential libssl-dev
RUN wget https://github.com/Kitware/CMake/releases/download/v3.20.1/cmake-3.20.1.tar.gz
RUN tar zxvf cmake-3.20.1.tar.gz
RUN cd cmake-3.20.1 && \
    cmake . && \
    make && \
    make install
RUN rm -rf cmake-3.20.1.tar.gz cmake-3.20.1

# Catch2 testing framework for C++
RUN git clone https://github.com/catchorg/Catch2.git && \
    cd Catch2 && \
    git checkout v2.13.6 && \
    cmake -Bbuild -H. -DBUILD_TESTING=OFF && \
    sudo cmake --build build/ --target install

# ImageMagick convert tool required to convert bitmaps to 24 bit B-G-R
RUN apt-get -y install imagemagick

# Latest Clang C++ compiler supports C++20
RUN echo "deb     http://apt.llvm.org/buster/ llvm-toolchain-buster-12 main" >> /etc/apt/sources.list && \
    echo "deb-src http://apt.llvm.org/buster/ llvm-toolchain-buster-12 main" >> /etc/apt/sources.list && \
    wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add - && \
    apt-get update && \
    apt-get -y install clang-12

# For compiling/testing on the host we use Clang 12.
# It supports C++20 which default GCC on Debian does not.
ENV CMAKE_C_COMPILER=clang-12
ENV CMAKE_CXX_COMPILER=clang++-12

# Coverage tool
RUN apt-get -y install lcov

# Code quality -- old version
RUN apt-get -y install cppcheck
